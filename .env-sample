# Optional logging settings, uncomment one of these example lines or add your own.
# See comments after each for more info.
# Default: "nexexp:app,nexexp:error"
#DEBUG=*  # Enable all logging, including middleware
#DEBUG=nexexp:*  # Enable all logging specific to nex-rpc-explorer
#DEBUG=nexexp:app,nexexp:error  # Default (you could add also nexexp:errorVerbose)

# The active coin. Only officially supported value is "NEX".
# Default: NEX
#NEXEXP_COIN=NEX

# Host/Port to bind to
# Defaults: shown
#NEXEXP_HOST=127.0.0.1
#NEXEXP_PORT=3002

# nexa RPC Credentials (URI -OR- HOST/PORT/USER/PASS)
# Defaults:
#   - [host/port]: 127.0.0.1:8332
#   - [username/password]: none
#   - cookie: '~/.nexa/.cookie'
#   - timeout: 5000 (ms)
#NEXEXP_NEXAD_URI=nexa://rpcusername:rpcpassword@127.0.0.1:8332?timeout=10000
#NEXEXP_NEXAD_HOST=127.0.0.1
#NEXEXP_NEXAD_PORT=7227
#NEXEXP_NEXAD_USER=rpcusername
#NEXEXP_NEXAD_PASS=rpcpassword
#NEXEXP_NEXAD_COOKIE=/path/to/nexad/.cookie
#NEXEXP_NEXAD_RPC_TIMEOUT=5000

# Select optional "address API" to display address tx lists and balances
# Options: electrumx, blockchain.com, blockchair.com, blockcypher.com
# If electrumx set, the NEXEXP_ELECTRUMX_SERVERS variable must also be
# set.
# Default: none
# So far there's no Electrum server implementation for NEXA
# Rostrum will fill the blank soon :P
#NEXEXP_ADDRESS_API=(electrumx|blockchain.com|blockcypher.com)

# Optional ElectrumX Servers. See NEXEXP_ADDRESS_API. This value is only
# used if NEXEXP_ADDRESS_API=electrumx
#NEXEXP_ELECTRUMX_SERVERS=tls://electrumx.server.com:50002,tcp://127.0.0.1:50001,...

# Set number of concurrent RPC requests. Should be lower than your node's "rpcworkqueue" value.
# Note that nexa's default rpcworkqueue=16.
# Default: 10
#NEXEXP_RPC_CONCURRENCY=10

# Disable app's in-memory RPC caching to reduce memory usage
# Default: false (i.e. in-memory cache **enabled**)
#NEXEXP_NO_INMEMORY_RPC_CACHE=true

# Optional redis server for RPC caching
# Default: none
#NEXEXP_REDIS_URL=redis://localhost:6379

# Default: hash of credentials
#NEXEXP_COOKIE_SECRET=0000aaaafffffgggggg

# Whether public-demo aspects of the site are active
# Default: false
#NEXEXP_DEMO=true

# Set to false to enable resource-intensive features, including:
# UTXO set summary querying
# (default value is true, i.e. resource-intensive features are disabled)
#NEXEXP_SLOW_DEVICE_MODE=false

# Privacy mode disables:
# Exchange-rate queries, IP-geolocation queries
# Default: false
#NEXEXP_PRIVACY_MODE=true

# Don't request currency exchange rates
# Default: true (i.e. no exchange-rate queries made)
#NEXEXP_NO_RATES=true

# Show RPC terminal and browser tools
# Default: false
#NEXEXP_UI_SHOW_RPC=true

# Password protection for site via basic auth (enter any username, only the password is checked)
# Default: none
#NEXEXP_BASIC_AUTH_PASSWORD=mypassword

# Enable to allow access to all RPC methods
# Default: false
#NEXEXP_RPC_ALLOWALL=true

# Custom RPC method blacklist
# Default: (see config.js)
#NEXEXP_RPC_BLACKLIST=signrawtransaction,sendtoaddress,stop,...

# Optional API keys
# Default: none
#NEXEXP_GANALYTICS_TRACKING=UA-XXXX-X
#NEXEXP_SENTRY_URL=https://00000fffffff@sentry.io/XXXX
#NEXEXP_IPSTACK_APIKEY=000000fffffaaaaa

# Optional value for "max_old_space_size"
# Default: 1024
#NEXEXP_OLD_SPACE_MAX_SIZE=2048

# Show tools list in a sub-nav at top of screen
# Default: true
#NEXEXP_UI_SHOW_TOOLS_SUBHEADER=true

# Does getblock support "height" as parameter?
# Default: false
#NEXEXP_BLOCK_BY_HEIGHT_SUPPORT=false

# Avoid showing IP addresses (ipv4, ipv6 and onion)
# Default: true
#NEXEXP_HIDE_IP=true

# Avoid shoing pug render error on views
# Default: true
#NEXEXP_SHOW_PUG_RENDER_STACKTRACE=true

